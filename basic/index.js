const http = require('http');
const uuid4 = require('uuid/v4');
const WebsocketServer = require('websocket').server;

const httpServer = http.createServer();
let wsServer;

const error_code = {
    'Invalid Limit Bid Price': 1,
    'Invalid Limit Offer Price': 2
}

function Pending(id, user, vol, price) {
    this.id = id
    this.user = user
    this.vol = vol 
    this.remaining = vol
    this.price = price
}

function Fill(makerId, takerId, vol, price) {
    this.makerId = makerId;
    this.takerId = takerId;
    this.vol = vol;
    this.price = price;
}

function FillError(code, message) {
    this.code = code;
    this.message = error_code[code];
}

function start() {
    httpServer.listen(9000, function() {
        console.log('http server listening on port 9000');
    })
    
    wsServer = new WebsocketServer({
        httpServer,
        autoAcceptConnections: false
    })

    wsServer.on('request', function(req) {
        if (!originIsAllowed(req.origin)) {
            return req.reject();
        }
    
        console.log('creating new connection ...');
    
        const conn = req.accept(null, req.origin);
    
        conn.on('message', function(msg) {
            const transaction = msg.utf8Data;
            const state = process(transaction, old_state);
            old_state = state; 
    
            console.log('state: ', state);
        })
    
        conn.on('close', function() {
            // console.log('connection closed');
        })
    })
}

function originIsAllowed(origin) {
    return true;
}

function processPlaceLimit(state, user_id, symbol, side, price, vol, transaction_id) {
   
    const priceTree = state[symbol][side].price;
    const transactionTree = state[symbol][side].id;

    const record = new Pending(transaction_id, user_id, vol, price);

    priceTree.store(price, record);
    transactionTree.store(transaction_id, record);

    return state;
}

function checkLimit(state, symbol, side, price) {
    const oppositeSide = (side == '0') ? '1' : '0';
    const priceTree = state[symbol][oppositeSide].price;
    const bestPriceNode = priceTree.values({key: priceTree.tree.k[0]}).next().value;
    let err = null;

    if (bestPriceNode) {
        const bestPrice = Number(bestPriceNode.k);
        if (side == '0') {
            if (price >= bestPrice) {
                return new FillError(1);
            }
        } else {
            if (price <= bestPrice) {
                return new FillError(2);
            }
        }
    } 

    return err;
}

function removeOrder(idTree, priceTree, id, fill_price, order) {
    function _removeOrder() {
        priceTree.remove(fill_price, order);
        idTree.remove(id, order);
    }
    return _removeOrder;
}

function process(tr, orderBookState) {
    const args = tr.split(',');
    const cmd = args[0];
    
    let err = null;
    let data = [];

    if (cmd == '0') {
        const user_id = args[1];
        const symbol = args[2]; 
        const side = args[3];
        const price = args[4];
        const vol = Number(args[5]);
        const transaction_id = args[6];
        let state;

        err = checkLimit(orderBookState, symbol, side, price);
        if (err)
            return [err, orderBookState, data];
        else {
            const new_state = processPlaceLimit(orderBookState, user_id, symbol, side, price, vol, transaction_id);
            return [err, new_state, data];
        }

    } else if (cmd == '1') 
    {
        const user_id = args[1];
        const symbol = args[2];
        const side = args[3];
        const vol = Number(args[4]);
        const order_id = args[5];
        let priceTree;
        let queue = [];

        priceTree = orderBookState[symbol][side].price;

        const gen = priceTree.values({key: priceTree.tree.k[0]});
        let filled_vol = 0;
        let node = gen.next().value;

        while (filled_vol < vol) {
            if (node == undefined)
                break;

            const fill_price = node.k;
            const orders = node.v;

            for (let i=0; i<orders.length; i++) {
                const fill_vol = Math.min(vol - filled_vol, orders[i].remaining);
                orders[i].remaining -= fill_vol;
                data.push(new Fill(orders[i].id, order_id, fill_vol, fill_price));
                filled_vol += fill_vol;

                //queue orders to delete when remaining volume drops to zero
                if (orders[i].remaining <= 0) {
                    const idTree = orderBookState[symbol][side].id;
                    queue.push(removeOrder(idTree, priceTree, orders[i].id, orders[i].price, orders[i]));
                }
            }

            node = gen.next().value;
        }

        //delete orders
        for (let i=0; i<queue.length; i++) {
            queue[i]();
        }
        
        return [err, orderBookState, data] 

    } else if (cmd == '2') {
        const transaction_id = args[1];
        const symbol = transaction_id[0] + transaction_id[1];
        const side = transaction_id[2];
        
        let transactionTree;
        let priceTree;
        if (side == 'b') {
            switch(symbol) {
                case 'eu': {
                    transactionTree = orderBookState['0']['0'].id;
                    priceTree = orderBookState['0']['0'].price;
                }
            }           
        } else {
            switch(symbol) {
                case 'eu': {
                    transactionTree = orderBookState['0']['1'].id;
                    priceTree = orderBookState['0']['1'].price;
                }
            }   
        }

        const order = transactionTree.fetch(transaction_id)[0];
        const price = order.price;
        transactionTree.remove(transaction_id, order);
        priceTree.remove(price, order);

        return [err, orderBookState, data];
    }
    else {

    }

    return state;
}

var old_state = {};

module.exports = {
    process,
    error_code
}