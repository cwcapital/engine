const chai = require('chai');
const assert = chai.assert;
const { placeLimit, cancelLimit, placeMarket, uuidGen } = require('../client/launch.js');
const { process, error_code } = require('../basic/index.js');
const uuid = require('uuid/v4');
const BPlusTree = require('bplustree');

function getPartialState(state, symbol) {
    if (symbol == 'EURUSD') {
        return {
            bidsPrice: state['0']['0'].price.repr(),
            bidsId: state['0']['0'].id.repr(),
            asksPrice: state['0']['1'].price.repr(),
            asksId: state['0']['1'].id.repr()
        }
    }
}
const order = 6;

function initialState() {
    return {
        '0': {
            '0': { 
                price: new BPlusTree({order, cmpFn: (a,b) => ((a > b) ? -1 : ((a < b) ? 1 : 0))}),
                id: new BPlusTree()
            }, 
            '1': {
                price: new BPlusTree(),
                id: new BPlusTree()
            }
        }
    }
}

describe('market orders', function() {
    describe('place buy', function() {
        it('wipe ask book price level', function() {
            let state = initialState();
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit orders 
            const trans_id_1 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, trans_id_1, 'EURUSD', 'sell', 1.8, 200);
            [err, state, data] = process(trans, state)

            const trans_id_2 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, trans_id_2, 'EURUSD', 'sell', 1.0, 100);
            [err, state, data] = process(trans, state)

            const trans_id_3 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, trans_id_3, 'EURUSD', 'sell', 1.0, 100);
            [err, state, data] = process(trans, state)
    
            // place market orders
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'buy', 250);
            [err, state, data] = process(trans, state);
            
            assert.equal(data.length, 3);
            assert.equal(data[0].makerId, trans_id_2);
            assert.equal(data[0].takerId, order_id);
            assert.equal(data[0].vol, 100);
            assert.equal(data[0].price, 1.0);

            assert.equal(data[1].makerId, trans_id_3);
            assert.equal(data[1].takerId, order_id);
            assert.equal(data[1].vol, 100);
            assert.equal(data[1].price, 1.0);
            
            assert.equal(data[2].makerId, trans_id_1);
            assert.equal(data[2].takerId, order_id);
            assert.equal(data[2].vol, 50);
            assert.equal(data[2].price, 1.8);
        })

        it('fills on best offer', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit buy order
            const transId_1 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId_1, 'EURUSD', 'sell', 1.7, 80);
            [err, state, data] = process(trans, state)

            const transId_2 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId_2, 'EURUSD', 'sell', 1.6, 80);
            [err, state, data] = process(trans, state)

            const transId_3 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId_3, 'EURUSD', 'sell', 1.6, 20);
            [err, state, data] = process(trans, state)

            const transId_4 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId_4, 'EURUSD', 'sell', 1.8, 80);
            [err, state, data] = process(trans, state)
    
            // place market sell order 
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'buy', 100);
            [err, state, data] = process(trans, state);

            assert.equal(data.length, 2);
            assert.equal(data[0].price, 1.6);
            assert.equal(data[0].vol, 80);
            assert.equal(data[1].price, 1.6);
            assert.equal(data[1].vol, 20);
        })

        it('normal fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit buy order
            const transId = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId, 'EURUSD', 'sell', 1.6, 80);
            [err, state, data] = process(trans, state)
    
            // place market sell order 
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'buy', 30);
            [err, state, data] = process(trans, state);
            
            eurState = getPartialState(state, 'EURUSD');
            let record = eurState.asksPrice[1.6][0];
            assert.equal(record.id, transId);
            assert.equal(record.vol, 80);
            assert.equal(record.remaining, 50);

            assert.equal(Object.keys(eurState.bidsPrice), 0);
        })

        it('partial fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();

            // place limit buy order
            const transId = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user1, transId, 'EURUSD', 'sell', 1.6, 60);
            [err, state, data] = process(trans, state)
            let trade_id, filled_qty;

            // place market sell order
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'buy', 70);
            [err, state, data] = process(trans, state);

            assert.equal(data.length, 1);
            assert.equal(data[0].makerId, transId);
            assert.equal(data[0].takerId, order_id);
            assert.equal(data[0].vol, 60);
            assert.equal(data[0].price, 1.6);
            
            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
            assert.equal(Object.keys(eurState.asksPrice).length, 0);
        })

        it('no fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user = uuid();

            const order_id = uuid();
            trans = placeMarket(user, order_id, 'EURUSD', 'buy', 100);
            [err, state, data] = process(trans, state);
    
            assert.equal(data.length, 0);

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice), 0);
            assert.equal(Object.keys(eurState.asksPrice), 0);
        })
    })

    describe('place sell', function() {
        it('wipe ask book price level', function() {
            let state = initialState();
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit orders 
            const trans_id_1 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, trans_id_1, 'EURUSD', 'buy', 1.7, 100);
            [err, state, data] = process(trans, state)

            const trans_id_2 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, trans_id_2, 'EURUSD', 'buy', 2.0, 100);
            [err, state, data] = process(trans, state)

            const trans_id_3 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, trans_id_3, 'EURUSD', 'buy', 1.2, 100);
            [err, state, data] = process(trans, state)
    
            // place market orders
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'sell', 250);
            [err, state, data] = process(trans, state);
            
            assert.equal(data.length, 3);
            assert.equal(data[0].makerId, trans_id_2);
            assert.equal(data[0].takerId, order_id);
            assert.equal(data[0].vol, 100);
            assert.equal(data[0].price, 2.0);
            
            assert.equal(data[1].makerId, trans_id_1);
            assert.equal(data[1].takerId, order_id);
            assert.equal(data[1].vol, 100);
            assert.equal(data[1].price, 1.7);

            assert.equal(data[2].makerId, trans_id_3);
            assert.equal(data[2].takerId, order_id);
            assert.equal(data[2].vol, 50);
            assert.equal(data[2].price, 1.2);
        })

        it('fills on best offer', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit buy order
            const transId_1 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId_1, 'EURUSD', 'buy', 1.7, 80);
            [err, state, data] = process(trans, state)

            const transId_2 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId_2, 'EURUSD', 'buy', 1.8, 80);
            [err, state, data] = process(trans, state)

            const transId_3 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId_3, 'EURUSD', 'buy', 1.5, 20);
            [err, state, data] = process(trans, state)

            const transId_4 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId_4, 'EURUSD', 'buy', 1.9, 20);
            [err, state, data] = process(trans, state)
    
            // place market sell order 
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'sell', 100);
            [err, state, data] = process(trans, state);

            assert.equal(data.length, 2);
            assert.equal(data[0].price, 1.9);
            assert.equal(data[0].vol, 20);
            assert.equal(data[1].price, 1.8);
            assert.equal(data[1].vol, 80);
        })

        it('normal fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();

            // place limit buy order
            const transId = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId, 'EURUSD', 'buy', 1.4, 100);
            [err, state, data] = process(trans, state)
            let trade_id, filled_qty;

            // place market sell order 
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'sell', 40);
            [err, state, data] = process(trans, state);
            
            eurState = getPartialState(state, 'EURUSD');
            let record = eurState.bidsPrice[1.4][0];
            assert.equal(record.id, transId);
            assert.equal(record.vol, 100);
            assert.equal(record.remaining, 60);

            assert.equal(Object.keys(eurState.asksPrice), 0);
        })

        it('partial fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user1 = uuid();
            const user2 = uuid();
    
            // place limit buy order
            const transId = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user1, transId, 'EURUSD', 'buy', 1.6, 60);
            [err, state, data] = process(trans, state)
            let trade_id, filled_qty;
    
            // place market sell order
            const order_id = uuid();
            trans = placeMarket(user2, order_id, 'EURUSD', 'sell', 70);
            [err, state, data] = process(trans, state);
    
            assert.equal(data.length, 1);
            assert.equal(data[0].makerId, transId);
            assert.equal(data[0].takerId, order_id);
            assert.equal(data[0].vol, 60);
            assert.equal(data[0].price, 1.6);
            
            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
        })

        it('no fill', function() {
            let state = initialState();
            let eurState;
            let trans;
            const user = uuid();

            const order_id = uuid();
            trans = placeMarket(user, order_id, 'EURUSD', 'sell ', 100);
            [err, state, data] = process(trans, state);
    
            assert.equal(data.length, 0);

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice), 0);
            assert.equal(Object.keys(eurState.asksPrice), 0);
        })
    })
})

describe('limit orders', function() {
    describe('prohibitions', function() {
        it('new bids must be less than best ask', function() {
            let state = initialState();
            let trans;
            const user = uuid();
    
            // place limit buy order
            const transId_1 = uuidGen(uuid(), 'EURUSD', 'ask');
            trans = placeLimit(user, transId_1, 'EURUSD', 'ask', 1.5, 60);
            [err, state, data] = process(trans, state)
            assert.isNull(err);

            const prevState = state;

            const transId_2 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user, transId_2, 'EURUSD', 'buy', 1.5, 100);
            [err, state, data] = process(trans, state)
            assert.equal(err.code, error_code['Invalid Limit Bid Price']);
            assert.equal(state, prevState);

            const transId_3 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user, transId_3, 'EURUSD', 'buy', 1.6, 100);
            [err, state, data] = process(trans, state)
            assert.equal(err.code, error_code['Invalid Limit Bid Price']);
            assert.equal(state, prevState);
        })

        it('new offers must be greater than best bid', function() {
            let state = initialState();
            let trans;
            const user = uuid();

            // place limit buy order
            const transId_1 = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(user, transId_1, 'EURUSD', 'buy', 1.5, 60);
            [err, state, data] = process(trans, state)
            assert.isNull(err);

            const prevState = state;

            const transId_2 = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user, transId_2, 'EURUSD', 'sell', 1.5, 100);
            [err, state, data] = process(trans, state)
            assert.equal(err.code, error_code['Invalid Limit Offer Price']);
            assert.equal(state, prevState);

            const transId_3= uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(user, transId_3, 'EURUSD', 'sell', 1.4, 100);
            [err, state, data] = process(trans, state)
            assert.equal(err.code, error_code['Invalid Limit Offer Price']);
            assert.equal(state, prevState);
        })
    })

    describe('operations', function() {
        it('place sell', function() {
            let state = initialState();
            let trans;
            const userId = uuid();
            
            const transId = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(userId, transId, 'EURUSD', 'sell', 1.4, 100);
            [err, state, data] = process(trans, state)

            // check global state space
            assert.equal(Object.keys(state).length, 1);
            assert.deepEqual(Object.keys(state), ['0']);        // 0: EURUSD 
            assert.equal(Object.keys(state['0']).length, 2);    // 0: bids, 1: asks

            // check EURUSD state space
            eurState = getPartialState(state, 'EURUSD');
    
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
            assert.equal(Object.keys(eurState.bidsId).length, 0);
            assert.equal(Object.keys(eurState.asksPrice).length, 1);
    
            const priceRecord = eurState.asksPrice[1.4];
            assert.equal(priceRecord.length, 1);
    
            const r = priceRecord[0];
            assert.equal(r.id, transId);
            assert.equal(r.user, userId);
            assert.equal(r.vol, 100);
            assert.equal(r.remaining, 100);
            assert.equal(r.price, 1.4)
    
            const transRecord = eurState.asksId[transId];
            assert.equal(transRecord.length, 1);

            assert.equal(r, transRecord[0]);
        })

        it('cancel sell', function() {
            let state = initialState();
            let eurState;
            let trans;
            const userId = uuid();
            
            const transId = uuidGen(uuid(), 'EURUSD', 'sell');
            trans = placeLimit(userId, transId, 'EURUSD', 'sell', 1.4, 100);
            [err, state, data] = process(trans, state)

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
            assert.equal(Object.keys(eurState.bidsId).length, 0);
            assert.equal(Object.keys(eurState.asksPrice).length, 1);
            assert.equal(Object.keys(eurState.asksId).length, 1);

            trans = cancelLimit(transId);
            [err, state, data] = process(trans, state)

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
            assert.equal(Object.keys(eurState.bidsId).length, 0);
            assert.equal(Object.keys(eurState.asksPrice).length, 0);
            assert.equal(Object.keys(eurState.asksId).length, 0);
        })

        it('cancel buy', function() {
            let state = initialState();
            let eurState;
            let trans;
            const userId = uuid();
            
            const transId = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(userId, transId, 'EURUSD', 'buy', 1.4, 100);
            [err, state, data] = process(trans, state)

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 1);
            assert.equal(Object.keys(eurState.bidsId).length, 1);
            assert.equal(Object.keys(eurState.asksPrice).length, 0);
            assert.equal(Object.keys(eurState.asksId).length, 0);

            trans = cancelLimit(transId);
            [err, state, data] = process(trans, state)

            eurState = getPartialState(state, 'EURUSD');
            assert.equal(Object.keys(eurState.bidsPrice).length, 0);
            assert.equal(Object.keys(eurState.bidsId).length, 0);
            assert.equal(Object.keys(eurState.asksPrice).length, 0);
            assert.equal(Object.keys(eurState.asksId).length, 0);
        })

        it('place buy', function() {
            let state = initialState();
            let trans;
            const userId = uuid();
            
            const transId = uuidGen(uuid(), 'EURUSD', 'buy');
            trans = placeLimit(userId, transId, 'EURUSD', 'buy', 1.4, 100);
            [err, state, data] = process(trans, state)

            // check global state space
            assert.equal(Object.keys(state).length, 1);
            assert.deepEqual(Object.keys(state), ['0']);        // 0: EURUSD 
            assert.equal(Object.keys(state['0']).length, 2);    // 0: bids, 1: asks

            // check EURUSD state space
            eurState = getPartialState(state, 'EURUSD');
    
            assert.equal(Object.keys(eurState.asksPrice).length, 0);
            assert.equal(Object.keys(eurState.asksId).length, 0);
            assert.equal(Object.keys(eurState.bidsPrice).length, 1);
    
            const priceRecord = eurState.bidsPrice[1.4];
            assert.equal(priceRecord.length, 1);
    
            const r = priceRecord[0];
            assert.equal(r.id, transId);
            assert.equal(r.user, userId);
            assert.equal(r.vol, 100);
            assert.equal(r.remaining, 100);
            assert.equal(r.price, 1.4)
    
            const transRecord = eurState.bidsId[transId];
            assert.equal(transRecord.length, 1);

            assert.equal(r, transRecord[0]);
        })
    })
})
