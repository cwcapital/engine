const WebSocketClient = require('websocket').client;
const client = new WebSocketClient();
 
client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log("Received: '" + message.utf8Data + "'");
        }
    });

    const msg = placeLimit('EURUSD', 'sell', 1.4, 100);

    connection.sendUTF(msg);

    connection.close();
})

function uuidGen(uuid, symbol, side) {
    let symPrefix;

    if (symbol == 'EURUSD') {
        symPrefix = 'eu';
    }

    if (side == 'buy') {
        return symPrefix + 'b' + uuid;
    } else {
        return symPrefix + 'a' + uuid;
    }
}

function cancelLimit(transaction_id) {
    const cmd = '2,';
    return cmd + transaction_id;
}

function placeMarket(user_id, transaction_id, symbol, side, vol) {
    const cmd = '1,';

    let msg = cmd + user_id + ',';

    if (symbol == 'EURUSD') {
        msg += '0,';
    }

    if (side == 'buy') {
        msg += '1,';
    } else {
        msg += '0,';
    }

    msg += String(vol) + ',';

    msg += transaction_id;

    return msg;
}

function placeLimit(user_id, transaction_id, symbol, side, price, vol) {
    const cmd = '0,';

    let msg = cmd + user_id + ',';

    if (symbol == 'EURUSD') {
        msg += '0,';
    }

    if (side == 'buy') {
        msg += '0,';
    } else {
        msg += '1,';
    }

    msg += String(price) + ',';

    msg += String(vol) + ',';

    msg += transaction_id;

    return msg;
}

function connect() {
    client.connect('ws://localhost:9000/', null);
}

module.exports = {
    uuidGen,
    placeLimit,
    placeMarket,
    cancelLimit
}